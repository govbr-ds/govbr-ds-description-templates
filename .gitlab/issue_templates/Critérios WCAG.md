# Critérios de Sucesso de Acessibilidade

## [Nome do Critério [Nível]](URL da WCAG22 relacionado ao critério)

- **Descrição:** Detalhe a descrição;
- **Motivação para Aplicação no Componente:** Detalhe a Motivação para aplicação no componente.

---

## Especificações de Acessibilidade

### Aspectos e Detalhes do Componente

1. **Atributos ARIA**:
   - Use o atributo `aria-hidden="true"` para que as tecnologias assistivas ignorem o ícone durante a interação.
2. **Role para o componente**
   - Não se aplica a este componente
3. **Rótulos e Descrições**
   - Não se aplica pois o componente será composto com outro componente que já possui tais especificações.
4. **Teclas de Navegação**
   - Não se aplica pois o componente será composto com outro componente que já possui tais especificações.
5. **Recomendações**
   - Sem recomendações adicionais
6. **Tag HTML semântico**
   - Normalmente se faz uso da tag SVG, contudo outros elementos são aceitos.
