<!--
- Esse template é de uso exclusivo do time interno do GOVBR-DS. Para abrir uma solicitação de demanda, utilize o template <Default>.
- Por favor não criar issues para fazer perguntas. Caso tenha dúvidas, pesquisa nossa wiki: <https://www.gov.br/ds/wiki/>
- Caso não consiga encontrar a informação que deseja na wiki, use nossos canais:
   - Discord: <https://discord.gg/U5GwPfqhUP>
-->

## Definição do Problema

<!-- Descreva a justificativa de forma clara, informando o problema ou necessidade que originou a demanda. Pode-se utilizar outros recursos, como imagens, para ilustrar a situação.

Caso a demanda seja emergencial deve ser citado também. -->

### Sugestão de solução

<!-- Coloque imagens, códigos ou descreve a sugestão da solução com as possíveis alternativas, caso existam -->

### Referências

<!-- Liste as referências de sites, aplicativos ou qualquer outro produto que tenha soluções sobre o problema descrito, caso existam -->

## Análise

### Risco da atividade

<!-- Listar e informar os riscos da não execução da demanda: Exemplo: tempo de execução, entendimento da solução, incompatibilidade tecnológica ou qualquer tipo de incerteza. -->

### Valor da atividade

<!-- Listar os resultados a ser alcançados a partir do atendimento dessa demanda. O que ela agrega de positivo ao projeto? Qual a relevância? -->

### Restrição da atividade

<!-- Caso haja algum impedimento ou a dependência de outra atividade deve ser listado aqui. -->

### Impactos

<!-- Listar e informar os componentes, artefatos, elementos, ou qualquer outro produto impactado por essa demanda. -->

### Atividades de criação

<!--
Listar e informar todas as tarefas que devem ser realizadas para aceitação da demanda.
Exemplo:
- [ ] Criar e atualizar a diretriz, solucionando os problemas detalhados
- [ ] Resolver os impactos
- [ ] Atualizar os UIKits
-->

### Atividades de implementação

<!--
Listar e informar todas as tarefas que devem ser realizadas para aceitação da demanda (obs: revisar as informações após etapa de criação).
Exemplo:
- [ ] Modificar o componente baseando-se nos ajustes de design
- [ ] Atualizar as entregas de design no site (diretriz, modelos...)
-->

<!-- Inclua as tags/etiquetas se que aplicam ao contexto desse MR -->

/label ~"fluxo::aberta"
