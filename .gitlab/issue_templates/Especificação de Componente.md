# [Nome do Componente]

> Todo componente precisa começar com o prefixo "br", exemplos: br-icon, br-tag.

O componente `[Nome do Componente]` é utilizado para [breve descrição do objetivo ou funcionalidade principal do componente - deve estar de acordo com "Objetivo" da issue de pesquisa do componente].

Elemento HTML:

```html
<br-component-name></br-component-name>
```

## Anatomia

> Manter os nomes ou termos usados nas especificações de design.

| Anatomia   | Estrutura HTML         | Descrição                                                                               |
| ---------- | ---------------------- | --------------------------------------------------------------------------------------- |
| Contêiner  | Tag `host` do elemento | O contêiner é implementado por meio de CSS. No HTML, corresponde à tag raiz do elemento |
| [Elemento] | [Tag HTML]             | [Descrição funcional do elemento]                                                       |

## Propriedades

| Atributo HTML   | Propriedade JS | Restrição       | Descrição             | Tipo   | Valores Aceitos   | Padrão    |
| --------------- | -------------- | --------------- | --------------------- | ------ | ----------------- | --------- |
| `prop-required` | `propRequired` | **OBRIGATÓRIO** | [Descrição detalhada] | `tipo` | [Valores aceitos] | `[valor]` |
| `prop-optional` | `propOptional` | *OPCIONAL*      | [Descrição detalhada] | `tipo` | [Valores aceitos] | `[valor]` |

**Propriedades JS** podem ser acessadas por meio do objeto HTMLElement que representa o elemento.

## Exemplos de Uso
```html
 <br-message title="Title" content="message component content" category="default" call-to-action-content="refresh">
</br-message>
```

## Slots

[Descrição dos slots disponíveis no componente. Caso não possua, mencionar explicitamente.]

| Slot     | Descrição                         |
| -------- | --------------------------------- |
| `[nome]` | [Descrição do slot, se aplicável] |

## Estados

> Caso o componente não possua estados, especificar: "O componente não possui estado próprio."

| Estado   | Descrição                                                          |
| -------- | ------------------------------------------------------------------ |
| `[nome]` | [Descrição do estado e seu impacto no comportamento do componente] |

## Eventos

> Caso o componente não possua eventos, especificar: "O componente não possui eventos."

| Evento             | Descrição             |
| ------------------ | --------------------- |
| `[nome-do-evento]` | [Descrição do evento] |

## Métodos

<!-- Liste e descreva os métodos públicos do componente, incluindo seus parâmetros e valores de retorno -->

| Métodos | Descrição | Parâmetros | Retorno |
| ------- | --------- | ---------- | ------- |
|         |           |            |         |

## Parts

<!-- Liste e descreva as partes do componente que podem ser estilizadas usando a pseudo-classe ::part -->

| Part | Descrição |
| ---- | --------- |
|      |           |
|      |           |
|      |           |

## Testes Unitários

<!-- Descreva as partes mais importantes do componente que devem ser cobertas por testes unitários -->

| Parte | Descrição |
| ----- | --------- |
|       |           |
|       |           |
|       |           |

## Testes E2E

<!-- Descreva as partes mais importantes do componente que devem ser cobertas por testes E2E -->

| Parte | Descrição |
| ----- | --------- |
|       |           |
|       |           |
|       |           |

## Subcomponentes

> Caso o componente não possua subcomponentes, especificar: "O componente não possui subcomponentes."

<!-- Liste e descreva quaisquer subcomponentes que são usados dentro deste componente -->

Cada subcomponentes tem um título específico para eles contendo propriedades, eventos, métodos, slots, acessibilidade e dependências.

## Acessibilidade

> Linkar para os critérios de sucesso para o componente.

---

## Dependências (internas e externas)

| Dependência   | Uso                |
| ------------- | ------------------ |
| [Dependência] | [Descrição do uso] |

## Breaking Changes

[Detalhamento de alterações importantes que podem afetar a compatibilidade. Caso não se aplique, mencionar explicitamente.]

---
