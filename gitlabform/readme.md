# GitlabForm

É uma ferramenta para facilitar a configuração do nosso repositório.

Acese a página dos desenvolvedores para detalhes de configuração: <https://gitlabform.github.io/gitlabform/>.

## Usando

1. Crie o token do gitlab e inclua no arquivo `config.yml`.
1. Instalar o gitlabform conforme a documentação dos desenvolvedores
1. Altere as configurações desejadas
1. Execute o comando abaixo:

```bash
gitlabform ALL_DEFINED -c config.yml
```
